import React from 'react';
import NavHeader from "../componets/NavHeader";
import ProductList from "./components/ProductList";

class Mall extends React.Component{

    render() {
        return(
            <div>
                <NavHeader />
                <ProductList />
            </div>
        );
    }
}

export default Mall;
