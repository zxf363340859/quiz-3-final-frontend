import React from 'react';
import './products.less';
import ProductDetail from "./ProductDetail";

class ProductList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            products: []
        }
        fetch("http://localhost:8080/api/products")
            .then(response => response.json())
            .then(response => {
                this.setState({
                    products: response
                });
            });

    }

    render() {
        return (
            <div className={'products'}>
                <ul className={'product-ul'}>
                    {
                        this.state.products.map((item, index) =>
                            <li className={'product-li'} key={index}>
                                <ProductDetail item={item}/>
                            </li>
                        )
                    }
                </ul>
            </div>
        );
    }
}

export default ProductList;
