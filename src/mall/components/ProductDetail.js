import React from 'react';
import './products.less';

class ProductDetail extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            ...this.props.item,
            disable:false
        }

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.setState({
            disable:true
        });
        fetch('http://localhost:8080/api/order', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.state)
        }).then((response) => {
            if(response.status == 201) {
                this.setState({
                    disable:false
                });
            }
        })
    }

    render() {
        return (
            <div>
                <img className={'product-img'} src={`http://localhost:8080/${this.props.item.url}.jpg`}/>
                <p className={'product-info'}>{this.props.item.name}</p>
                <p className={'product-info'}>单价:{this.props.item.price}/{this.props.item.unit}</p>
                <button className={'product-add'} onClick={this.handleClick} disabled={this.state.disable}>添加</button>
            </div>
        );

    }
}

export default ProductDetail;
