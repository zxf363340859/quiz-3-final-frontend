import React from 'react';
import NavHeader from "../componets/NavHeader";
import AddProduct from "./components/AddProduct";

class Manage extends React.Component{

    constructor(props) {
        super(props);
        this.handleBack = this.handleBack.bind(this);
    }

    handleBack() {
        this.props.history.push('/');
    }

    render() {
        return(
            <div>
                <NavHeader />
                <AddProduct back={this.handleBack}/>
            </div>
        );
    }
}

export default Manage;
