import React from 'react';
import './addProduct.less';

const Button = (props) => {
    return (
        <div>
            <button className={'add-button'} onClick={props.handleSubmit} disabled={props.disable}>提交</button>
        </div>
    );
}

export default Button;
