import React from 'react';
import './addProduct.less';
import Button from "./Button";

class AddProduct extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            price: '',
            unit: '',
            url: '',
            disable: true
        }
        this.handleName = this.handleName.bind(this);
        this.handlePrice = this.handlePrice.bind(this);
        this.handleUnit = this.handleUnit.bind(this);
        this.handleUrl = this.handleUrl.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.isDisable = this.isDisable.bind(this);
    }

    isDisable() {
        if(this.state.name !== '' && this.state.price !== '') {
            this.setState({
                disable: false
            });
        } else {
            this.setState({
                disable: true
            });
        }
    }

    handleName(event) {
        this.setState({
            name: event.target.value
        }, () => this.isDisable());
    }


    handlePrice(event) {
        this.setState({
            price: event.target.value
        }, () => this.isDisable());

    }
    handleUnit(event) {
        this.setState({
            unit: event.target.value
        });
    }
    handleUrl(event) {
        this.setState({
            url: event.target.value
        });
    }

    handleSubmit() {
        //todo
        fetch('http://localhost:8080/api/product', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.state)
        }).then((response) => {
            if(response.status === 409) {
                alert("商品已存在");
            } else {
                this.props.back();
            }
        });
    }


    render() {
        return(
            <div className={'add-product'}>
                <h1 className={'add-h1'}>添加商品</h1>
                <ul className={'add-ul'}>
                    <li>
                        <section>
                            <p>名称:</p>
                            <input className={'add-input'} onChange={this.handleName}/>
                        </section>
                    </li>
                    <li>
                        <section>
                            <p>价格</p>
                            <input className={'add-input'} onChange={this.handlePrice} type="number"/>
                        </section>
                    </li>
                    <li>
                        <section>
                            <p>单位</p>
                            <input className={'add-input'} onChange={this.handleUnit}/>
                        </section>
                    </li>
                    <li>
                        <section>
                            <p>图片</p>
                            <input className={'add-input'} onChange={this.handleUrl}/>
                        </section>
                    </li>
                </ul>
                <button className={'add-button'} onClick={this.handleSubmit} disabled={this.state.disable}>提交</button>
            </div>
        );
    }
}

export default AddProduct;
