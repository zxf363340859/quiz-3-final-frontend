import React from 'react';
import {Link} from "react-router-dom";
import './navHeader.less';

class NavHeader extends React.Component{

    render() {
        return(
            <div className='nav'>
               <ul className={'header-ul'}>
                   <li className={'header-li'}><Link to={'/mall'}><span>商城</span></Link></li>
                   <li className={'header-li'}><Link to={'/order'}><span>订单</span></Link></li>
                   <li className={'header-li'}><Link to={'/manage'}><span>添加商品</span></Link></li>
               </ul>
            </div>
        );
    }
}

export default NavHeader;
