import React from 'react';
import {Link} from "react-router-dom";
import './footer.less';

class Footer extends React.Component{

    render() {
        return(
            <div className={'footer'}>
               by Tristan
            </div>
        );
    }
}

export default Footer;
