import React from 'react';
import NavHeader from "../componets/NavHeader";
import UserOrder from "./components/UserOrder";

class Order extends React.Component{

    render() {
        return(
            <div>
                <NavHeader />
                <UserOrder />
            </div>
        );
    }
}

export default Order;
