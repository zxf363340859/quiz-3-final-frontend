import React from 'react';
import './userOrder.less'

class OrderDetail extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            id: this.props.item.id
        }
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleDelete() {
        fetch('http://localhost:8080/api/order', {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.state)
        }).then(response => {
                if(response.status == 500) {
                    alert("商品删除失败，请稍后再试")
                }
                this.props.handleChange();
            }
        )
    }

    render() {
        console.log('this id', this.props.item.id);
        return (
            <tr key={this.props.key}>
                <td key={this.props.key + 1}>{this.props.item.product.name}</td>
                <td key={this.props.key + 2}>{this.props.item.product.price}</td>
                <td key={this.props.key + 3}>{this.props.item.quantity}</td>
                <td key={this.props.key + 4}>{this.props.item.product.unit}</td>
                <td key={this.props.key + 5}>
                    <button onClick={this.handleDelete}>删除</button>
                </td>
            </tr>

        );
    }
}

export default OrderDetail;
