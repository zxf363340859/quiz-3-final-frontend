import React from 'react';
import './userOrder.less'
import ProductDetail from "../../mall/components/ProductDetail";
import OrderDetail from "./OrderDetail";
import {Link} from "react-router-dom";

class UserOrder extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            orders: ''
        };
        fetch("http://localhost:8080/api/orders")
            .then(response => response.json())
            .then(response => {
                    this.setState({
                        orders: response
                    })
                }
            )
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange() {
        fetch("http://localhost:8080/api/orders")
            .then(response => response.json())
            .then(response => {
                this.setState({
                    orders: response
                })
            });
    }


    render() {
        return (
            <div className={'orders'}>
                {
                    this.state.orders != '' ?
                        <table>
                            <tr className={'order-table-header'}>
                                <th>名字</th>
                                <th>单价</th>
                                <th>数量</th>
                                <th>单位</th>
                                <th>删除</th>
                            </tr>
                            {
                                this.state.orders.map((item, index) =>
                                    <OrderDetail handleChange={this.handleChange} item={item} key={index}></OrderDetail>
                                )
                            }
                        </table> :
                        <p>暂无订单，返回<Link to={'/'}>商城页面</Link>继续购买</p>
                }

            </div>
        );
    }
}

export default UserOrder;
