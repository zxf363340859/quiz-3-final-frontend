import React, {Component} from 'react';
import './App.less';
import {BrowserRouter as Router} from "react-router-dom";
import {Route, Switch} from "react-router";
import Manage from "./manage/Manage";
import Mall from "./mall/Mall";
import Order from "./order/order";

class App extends Component {
    render() {
        return (
            <div className='App'>
                <Router>
                    <Switch>
                        <Route path={"/manage"} component={Manage}></Route>
                        <Route path={"/order"} component={Order}></Route>
                        <Route path={"/"} component={Mall}></Route>
                    </Switch>
                </Router>
            </div>
        );
    }
}

export default App;
